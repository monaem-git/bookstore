package com.openapp.bookstore.bootstrap;

import com.openapp.bookstore.dao.AuthorRepository;
import com.openapp.bookstore.dao.BookRepository;
import com.openapp.bookstore.dao.CategoryRepository;
import com.openapp.bookstore.entities.Author;
import com.openapp.bookstore.entities.Book;
import com.openapp.bookstore.entities.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.stream.Stream;

@Component
public class SpringBootInitialize implements ApplicationListener<ContextRefreshedEvent> {

    private AuthorRepository authorRepository;
    private BookRepository bookRepository;
    private CategoryRepository categoryRepository;
    
    //Injectionde de dépendences par setters:

    @Autowired
    public void setAuthorRepository(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {

        categoryRepository.deleteAll();
        bookRepository.deleteAll();
        authorRepository.deleteAll();

        Stream.of("C1 Romantic Book1 Book2 Monaem","C2 Horror Book3 Book4 Morsi","C3 Documentation Book5 Book6 Mansour").forEach( c -> {
            Category ct = categoryRepository.save(new Category(c.split(" ")[0],c.split(" ")[1]));
            Book book1 = bookRepository.save(new Book(null,c.split(" ")[2], Math.random()*100,ct));
            Book book2 = bookRepository.save(new Book(null,c.split(" ")[3], Math.random()*100,ct));
            ArrayList<Book> books = new ArrayList<>();
            books.add(book1);
            books.add(book2);
            authorRepository.save(new Author(null,c.split(" ")[4],c.split(" ")[4]+"@ensi-uma.tn",books));
        });
    }
}
