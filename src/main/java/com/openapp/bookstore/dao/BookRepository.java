package com.openapp.bookstore.dao;

import com.openapp.bookstore.entities.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface BookRepository extends MongoRepository<Book,String> {
}
